<?php /* Smarty version 2.6.18, created on 2018-07-09 12:15:06
         compiled from default/player_left_column_mod.html */ ?>
<?php require_once(SMARTY_CORE_DIR . 'core.load_plugins.php');
smarty_core_load_plugins(array('plugins' => array(array('modifier', 'escape', 'default/player_left_column_mod.html', 2, false),)), $this); ?>
<div class="ps-column-frame<?php if ($this->_tpl_vars['shades']['s_modactions']): ?> s-closed<?php endif; ?>">
<div class="ps-column-header"><a href="" onclick="return false"><span><?php echo ((is_array($_tmp=$this->_tpl_vars['mod_actions_title'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</span></a></div>
<div id="s-modactions" class="ps-column-content">
<?php $_from = $this->_tpl_vars['mod_actions']; if (!is_array($_from) && !is_object($_from)) { settype($_from, 'array'); }if (count($_from)):
    foreach ($_from as $this->_tpl_vars['a']):
?>
<p>
	<label><?php echo ((is_array($_tmp=$this->_tpl_vars['a']['label'])) ? $this->_run_mod_handler('escape', true, $_tmp) : smarty_modifier_escape($_tmp)); ?>
</label>
	<span><?php echo $this->_tpl_vars['a']['value']; ?>
</span>
</p>
<?php endforeach; endif; unset($_from); ?>
</div>
</div>